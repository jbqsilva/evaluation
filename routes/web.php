<?php

/**
 * @OA\Info(
 *   title="Evaluation API",
 *   version="1.0",
 *   @OA\Contact(
 *     email="jefferson@example.com",
 *     name="Support Team (Jefferson)"
 *   )
 * )
 */

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/**
 * @OA\Post(
 *     path="/evaluate",
 *     description="Evaluate students",
 *     @OA\RequestBody(
 *          @OA\JsonContent(
 *              type="array",
 *              example={{
 *                      "name":"John",
 *                      "grade": 53
 *                  }, {
 *                      "name":"Jane",
 *                      "grade": 68
 *                  },{
 *                      "name":"Emma",
 *                      "grade": 32
 *                  },{
 *                      "name":"Sophia",
 *                      "grade": 39
 *                  }},
 *               @OA\Items(
 *                  @OA\Property(
 *                      property="name",
 *                      type="string",
 *                      description="student name",
 *                  ),
 *                  @OA\Property(
 *                      property="grade",
 *                      type="integer",
 *                      description="student grade"
 *                  ),
 *              ),
 *          ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="successful operation",
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                  @OA\Property(
 *                      property="name",
 *                      type="string",
 *                      description="student name",
 *                  ),
 *                  @OA\Property(
 *                      property="grade",
 *                      type="integer",
 *                      description="student grade"
 *                  ),
 *                  @OA\Property(
 *                      property="pass",
 *                      type="boolean",
 *                      description="student result"
 *                  ),
 *              ),
 *         )
 *     ),
 *     @OA\Response(
 *        response=404,
 *        description="Not Found",
 *        @OA\JsonContent(
 *              oneOf={
 *                  @OA\Schema(
 *                          schema="resourcenotfound",
 *                          title="Resource Not Found",
 *                          @OA\Property(
 *                              property="code",
 *                              type="integer",
 *                              example=404
 *                          ),
 *                          @OA\Property(
 *                              property="error",
 *                              type="string",
 *                              example="Resource not found"
 *                          )
 *                  ),
 *              }
 *        )
 *    ),
 *    @OA\Response(
 *        response=400,
 *        description="Bad Request",
 *        @OA\JsonContent(
 *              oneOf={
 *                  @OA\Schema(
 *                          title="Bad Request",
 *                          @OA\Property(
 *                              property="code",
 *                              type="integer",
 *                              example=400
 *                          ),
 *                          @OA\Property(
 *                              property="error",
 *                              type="string",
 *                              example="Bad Request"
 *                          )
 *                  ),
 *              }
 *        )
 *    ),
 *    @OA\Response(
 *        response=422,
 *        description="Unprocessable Entity",
 *        @OA\JsonContent(
 *             @OA\Property(
 *               property="errors",
 *               type="object",
 *               @OA\Property (
 *                  property="0.name",
 *                  type="array",
 *                  example={"The 0.name field is required"},
 *                  @OA\Items()
 *               ),
 *               @OA\Property (
 *                  property="0.grade",
 *                  type="array",
 *                  example={"The 0.grade field is required"},
 *                  @OA\Items()
 *               )
 *             )
 *        )
 *    ),
 * )
 */
$router->post('/evaluate', 'EvaluateController@evaluate');
