# Student Grade Evaluation

This project is an example of how to implement Lumen (fast PHP micro-framework) using PHP 8, Docker and REST API to process a list of student grades following the rules:
- Every student receives a grade from 0 to 100
- Any grade less than 35 is a failing grade (pass = false)
- Round each student grade
  - If the difference between the grade and the next multiple of 5 is less than 3, round grade up to the next multiple of 5

## Setup
```bash
# checkout repository
user@local ~ % git clone git@gitlab.com:jbqsilva/evaluation.git
user@local ~ % cd evaluation

# start docker
user@local ~/evaluation % cd docker
user@local ~/evaluation/docker % docker-compose up -d --build
user@local ~/evaluation % cd ..

# create config file (.env)
user@local ~/evaluation % cp .env.example .env

# install composer packages
user@local ~/evaluation % docker exec eval-php composer install
```

The API is now available via HTTPS on https://127.0.0.1:8002 or HTTP on http://127.0.0.1:8001

A postman collection is available in docs folder. 

## How to use it

Only one endpoint is available ```/evaluate```. 

1. Send the payload to the endpoint ```/evaluate``` using the HTTP method ```POST```:

Request payload example:
```json
[
   { "name": "John", "grade": 53 },
   { "name": "Jane", "grade": 68 },
   { "name": "Emma", "grade": 32 },
   { "name": "Sophia", "grade": 39 }
]
```
2. Get the response:

Response payload example:
```json
[
  { "name": "John", "grade": 55, "pass": true }, 
  { "name": "Jane", "grade": 70, "pass": true }, 
  { "name": "Emma", "grade": 30, "pass": false }, 
  { "name": "Sophia", "grade": 40, "pass": true }
]
```


## Documentation
```bash
# generate swagger documentation
user@local ~/evaluation % docker exec eval-php composer doc:generate
```

The documentation is available via HTTPS on https://127.0.0.1:8002/api/documentation or HTTP on http://127.0.0.1:8001/api/documentation

## Code quality
```bash
# Complete code quality sweep
user@local ~/evaluation % docker exec eval-php composer code:quality

# For individual check:

# lint
user@local ~/evaluation % docker exec eval-php composer code:lint

# lint fix
user@local ~/evaluation % docker exec eval-php composer code:lint:fix

# mess detection
user@local ~/evaluation % docker exec eval-php composer code:clean

# html mess detection report
user@local ~/evaluation % docker exec eval-php composer code:clean:html

# docBlock
user@local ~/evaluation % docker exec eval-php composer code:docblock
```

## Unit Tests
```bash
# run unit tests
user@local ~/evaluation % docker exec eval-php composer test

# generate code coverage report
user@local ~/evaluation % docker exec eval-php composer test:report
```

The Code Coverage Report is generated in the reports' folder.

## Troubleshooting

### Google Chrome Browser
Recently, the Chrome browser is blocking self-signed SSL certificates and the developer needs to explicitly allow 
requests from localhost or 127.0.0.1 to use those certificates.

```bash
# steps to allow self-signed certificates
1. Open Chrome Browser.
2. Type chrome://flags
3. Search for: "Allow invalid certificates for resources loaded from localhost"
4. Select Enabled.
5. Relaunch the Chrome Browser. 
```



