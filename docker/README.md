# Docker
Use the `docker-compose.yml` file for the local docker setup.
Services can either be added directly via image or with a locally built 
image.

If a locally built image is needed (e.g. PHP), create a separate folder
for the `Dockerfile` and all its configuration.

For all other environments but the local one suffix the `Dockerfile` with
the environment, e.g. `Dockerfile-dev` & `Dockerfile-stage` etc. This gives
you the opportunity to e.g. copy the source files into the container (not
needed locally due to local mounts).
