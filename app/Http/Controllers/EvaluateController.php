<?php

namespace App\Http\Controllers;

use App\Services\EvaluateService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class EvaluateController extends BaseController
{

    private EvaluateService $evaluateService;

    public function __construct(EvaluateService $evaluateService)
    {
        $this->evaluateService = $evaluateService;
    }

    public function evaluate(Request $request): JsonResponse
    {
        $data = $request->all();
        if (empty($data) || !is_array($data)) {
            return response()->json(['code' => 400, 'error' => 'Bad Request'], 400);
        }
        $responseData = $this->evaluateService->evaluateCollection($data);

        if (array_key_exists('errors', $responseData)) {
            return response()->json($responseData, 422);
        }
        return response()->json($responseData);
    }
}
