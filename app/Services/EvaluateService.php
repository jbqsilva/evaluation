<?php

namespace App\Services;

use App\Traits\RoundUp;
use Validator;

class EvaluateService
{
    use RoundUp;

    public function evaluateCollection($data = []): array
    {
        $validator = Validator::make($data, [
            '*.name' => 'required|string',
            '*.grade' => 'required|integer|min:0|max:100',
        ]);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }
        return array_map(function ($item) {
            return $this->evaluate($item);
        }, $data);
    }

    public function evaluate($data = []): array
    {
        $rules = [
            'name' => 'required',
            'grade' => 'required|integer|min:0|max:100',
        ];
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return ['errors' => $validator->errors()];
        }
        return [
            'name' => $data['name'],
            'grade' => $this->roundUpToAny($data['grade']),
            'pass' => $this->roundUpToAny($data['grade']) >= 35 ?? false,
        ];
    }
}
