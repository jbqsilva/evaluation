<?php

namespace App\Traits;

trait RoundUp
{
    public function roundUpToAny($num, $mul = 5)
    {
        return (ceil($num) % $mul < 3) ? round(($num - $mul / 2) / $mul) * $mul :
            round(($num + $mul / 2) / $mul) * $mul;
    }
}
