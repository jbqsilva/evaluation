<?php

namespace Tests;

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    protected function payloadSingleDataPass()
    {
        return [['name' => 'John', 'grade' => 53]];
    }

    protected function payloadSingleDataFail()
    {
        return [['name' => 'Emma', 'grade' => 32]];
    }

    protected function payloadMultipleData()
    {
        return [
            ['name' => 'John', 'grade' => 53],
            ['name' => 'Jane', 'grade' => 68],
            ['name' => 'Emma', 'grade' => 32],
            ['name' => 'Sophia', 'grade' => 39]
        ];
    }
}
