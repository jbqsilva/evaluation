<?php

namespace Tests\Unit;

use App\Services\EvaluateService;
use Tests\TestCase;

class EvaluateTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testPayloadSingleSuccessPassTrue()
    {
        $response = $this->post('/evaluate', $this->payloadSingleDataPass());

        $response->assertResponseStatus(200);
        $response->seeJsonEquals(
            [
                ['name' => 'John', 'grade' => 55, 'pass' => true]
            ]
        );
    }

    public function testPayloadSingleSuccessPassFalse()
    {
        $response = $this->post('/evaluate', $this->payloadSingleDataFail());

        $response->assertResponseStatus(200);
        $response->seeJsonEquals(
            [
                ['name' => 'Emma', 'grade' => 30, 'pass' => false]
            ]
        );
    }

    public function testPayloadMultipleSuccess()
    {
        $response = $this->post('/evaluate', $this->payloadMultipleData());

        $response->assertResponseStatus(200);
        $response->seeJsonEquals(
            [
                ['name' => 'John', 'grade' => 55, 'pass' => true],
                ['name' => 'Jane', 'grade' => 70, 'pass' => true],
                ['name' => 'Emma', 'grade' => 30, 'pass' => false],
                ['name' => 'Sophia', 'grade' => 40, 'pass' => true]
            ]
        );
    }

    public function testPayloadSingleFailing()
    {
        $response = $this->post('/evaluate', [[]]);

        $response->assertResponseStatus(422);
        $response->seeJsonEquals(
            [
                'errors' => [
                    "0.grade" => ["The 0.grade field is required."],
                    "0.name" => ["The 0.name field is required."]
                ]
            ]
        );
    }

    public function testPayloadServiceSuccessPassTrue()
    {
        $evaluateService = $this->app->make(EvaluateService::class);
        $data = $evaluateService->evaluate(['names' => 'john', 'grades' => 53]);
        $this->assertArrayHasKey('errors', $data);
    }
}
